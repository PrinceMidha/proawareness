package com.example.princemidha.proawareness;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.princemidha.proawareness.AsyncProcess.DBAsyncDeleteProcess;
import com.example.princemidha.proawareness.AsyncProcess.DBAsyncGetProcess;
import com.example.princemidha.proawareness.AsyncProcess.DBAsyncProcess;
import com.example.princemidha.proawareness.adapters.ContactsListAdapter;
import com.example.princemidha.proawareness.interfaces.AdapterCommunication;
import com.example.princemidha.proawareness.interfaces.WorkerToUI;
import com.example.princemidha.proawareness.models.ContactList;
import com.example.princemidha.proawareness.models.ResponseData;
import com.example.princemidha.proawareness.retrofit.RetrofitConstants;
import com.example.princemidha.proawareness.retrofit.RetrofitQueryCreator;
import com.example.princemidha.proawareness.retrofit.interfaces.RetrofitResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static com.example.princemidha.proawareness.retrofit.RetrofitConstants.MODE.CONTACTS_LIST;


public class MainActivity extends AppCompatActivity implements RetrofitResponse, AdapterCommunication, WorkerToUI {

    private RecyclerView rv_ContactList;
    private LinearLayoutManager mLayoutManager;
    private ContactsListAdapter mAdapter;
    private List<ContactList> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contactList = new ArrayList<ContactList>();
        rv_ContactList = (RecyclerView)findViewById(R.id.rv_ContactList); 
        
        mLayoutManager = new LinearLayoutManager(this);
        rv_ContactList.setLayoutManager(mLayoutManager);

        mAdapter = new ContactsListAdapter(contactList);
        rv_ContactList.setAdapter(mAdapter);
        RetrofitQueryCreator.getCreator().createQuery(this, this,CONTACTS_LIST, true);
    }

    @Override
    public void onFailure(Response response, RetrofitConstants.MODE mode) {

    }

    @Override
    public void onSuccess(Response response, RetrofitConstants.MODE mode) {
        if (AppUtils.isValid(this,response.body())) {
            ResponseData responseData = (ResponseData) response.body();
            new DBAsyncProcess(this,this).execute(responseData.getResult());
        }
    }

    @Override
    public void getRemovedPosition(int position) {
        ContactList contact = contactList.get(position);
        contact.setIsStatus(0);
        new DBAsyncDeleteProcess(this,this,position).execute(contact);
    }

    @Override
    public void doneInBackground(long status) {
        if (status == 200) {
            new DBAsyncGetProcess(this,this).execute();
        }
    }

    @Override
    public void gotListFromDB(List<ContactList> list) {
        if (null != list && list.size() > 0) {
            contactList.clear();
            contactList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void removedFromDB(int position) {
        contactList.remove(position);
        mAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRangeChanged(position, contactList.size());
    }
}
