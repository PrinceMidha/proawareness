package com.example.princemidha.proawareness.AsyncProcess;

import android.content.Context;
import android.os.AsyncTask;

import com.example.princemidha.proawareness.database.AppDatabase;
import com.example.princemidha.proawareness.database.DatabaseHelper;
import com.example.princemidha.proawareness.interfaces.WorkerToUI;
import com.example.princemidha.proawareness.models.ContactList;

import java.util.List;

/**
 * Created by princemidha on 07/07/17.
 */

public class DBAsyncGetProcess extends AsyncTask<Void,Void,List<ContactList>> {
    private AppDatabase database;
    private WorkerToUI workerToUI;
    private Context context;

    public DBAsyncGetProcess(Context context,WorkerToUI workerToUI) {
        this.workerToUI = workerToUI;
        this.context = context;
        database = DatabaseHelper.getInstance(context).getDb();
    }

    @Override
    protected List<ContactList> doInBackground(Void... params) {
        return database.contactListDao().getContactList();
    }

    @Override
    protected void onPostExecute(List<ContactList> list) {
        super.onPostExecute(list);
        workerToUI.gotListFromDB(list);
    }
}
