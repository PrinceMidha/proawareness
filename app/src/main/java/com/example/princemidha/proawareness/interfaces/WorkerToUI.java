package com.example.princemidha.proawareness.interfaces;

import com.example.princemidha.proawareness.models.ContactList;

import java.util.List;

/**
 * Created by princemidha on 30/05/17.
 */

public interface WorkerToUI {
    public void doneInBackground(long status);
    public void gotListFromDB(List<ContactList>list);
    public void removedFromDB(int position);
}
