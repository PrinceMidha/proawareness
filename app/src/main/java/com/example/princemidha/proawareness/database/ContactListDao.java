package com.example.princemidha.proawareness.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.princemidha.proawareness.models.ContactList;

import java.util.List;

/**
 * Created by PrinceMidha on 07/07/17.
 */

@Dao
public interface ContactListDao {
    @Query("Select * from Contacts where status > 0")
    public List<ContactList> getContactList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public long[] insertAll(List<ContactList> contactLists);

    @Update
    public int updateUsers(ContactList... contactLists);
}
