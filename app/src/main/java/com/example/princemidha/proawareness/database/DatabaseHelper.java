package com.example.princemidha.proawareness.database;

import android.arch.persistence.room.Room;
import android.content.Context;

/**
 * Created by PrinceMidha on 07/07/17.
 */

public class DatabaseHelper {
    private static DatabaseHelper ourInstance;
    private static AppDatabase db;

    private DatabaseHelper(){}
    public static DatabaseHelper getInstance(Context context) {

        if (ourInstance == null) {
            ourInstance = new DatabaseHelper();
            db = Room.databaseBuilder(context,
                    AppDatabase.class, "Proawareness").build();
        }
        return ourInstance;
    }

    public AppDatabase getDb() {
        return db;
    }
}
