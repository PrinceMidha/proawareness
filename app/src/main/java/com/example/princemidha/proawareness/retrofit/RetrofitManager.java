package com.example.princemidha.proawareness.retrofit;



import com.example.princemidha.proawareness.retrofit.interfaces.RetrofitService;

import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.princemidha.proawareness.retrofit.RetrofitConstants.BASE_URL;


/**
 * Created by PrinceMidha on 07/07/17.
 */
public class RetrofitManager {

    private Retrofit mRetrofit;
    private RetrofitService restService;
    private static RetrofitManager mRetrofitManager;

    public static RetrofitManager init() {
        if (null == mRetrofitManager) {
            mRetrofitManager = new RetrofitManager();
            mRetrofitManager.setRestAdaptor();
        }
        return mRetrofitManager;
    }

    private Retrofit getRestAdapter() {
        return (mRetrofit == null) ? setRestAdaptor() : mRetrofit;
    }

    public RetrofitService getRetrofitService() {
        return (restService == null) ? getRestAdapter().create(RetrofitService.class) : restService;
    }


    private Retrofit setRestAdaptor() {

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.connectTimeout(50, TimeUnit.SECONDS);
        builder.writeTimeout(50, TimeUnit.SECONDS);
        builder.readTimeout(50, TimeUnit.SECONDS);
        builder.retryOnConnectionFailure(true);
        builder.connectionPool(new ConnectionPool(4,5,TimeUnit.SECONDS));

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY );
        builder.addInterceptor(logging);
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        return mRetrofit;
    }
}
