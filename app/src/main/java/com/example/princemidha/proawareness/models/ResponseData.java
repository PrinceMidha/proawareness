package com.example.princemidha.proawareness.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by princemidha on 07/07/17.
 */

public class ResponseData extends BaseModel{
    @SerializedName("result")
    @Expose
    private List<ContactList> result = null;

    public List<ContactList> getResult() {
        return result;
    }

    public void setResult(List<ContactList> result) {
        this.result = result;
    }
}
