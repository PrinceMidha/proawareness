package com.example.princemidha.proawareness.AsyncProcess;

import android.content.Context;
import android.os.AsyncTask;

import com.example.princemidha.proawareness.database.AppDatabase;
import com.example.princemidha.proawareness.database.DatabaseHelper;
import com.example.princemidha.proawareness.interfaces.WorkerToUI;
import com.example.princemidha.proawareness.models.ContactList;

import java.util.List;

/**
 * Created by princemidha on 07/07/17.
 */

public class DBAsyncDeleteProcess extends AsyncTask<ContactList,Void,Integer> {
    private AppDatabase database;
    private WorkerToUI workerToUI;
    private Context context;
    private int position;

    public DBAsyncDeleteProcess(Context context, WorkerToUI workerToUI, int position) {
        this.context = context;
        this.position = position;
        this.workerToUI = workerToUI;
        database = DatabaseHelper.getInstance(context).getDb();
    }

    @Override
    protected Integer doInBackground(ContactList... params) {
        return database.contactListDao().updateUsers(params[0]);
    }

    @Override
    protected void onPostExecute(Integer pos) {
        super.onPostExecute(pos);
        workerToUI.removedFromDB(position);
    }
}
