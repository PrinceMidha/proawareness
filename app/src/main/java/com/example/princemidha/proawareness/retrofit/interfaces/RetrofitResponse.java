package com.example.princemidha.proawareness.retrofit.interfaces;




import com.example.princemidha.proawareness.retrofit.RetrofitConstants;

import retrofit2.Response;

/**
 * Created by PrinceMidha on 07/07/17.
 */
public interface RetrofitResponse<T> {

     void onFailure(Response<T> response, RetrofitConstants.MODE mode);

     void onSuccess(Response<T> response, RetrofitConstants.MODE mode);

}
