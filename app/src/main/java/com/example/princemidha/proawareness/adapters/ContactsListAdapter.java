package com.example.princemidha.proawareness.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.princemidha.proawareness.R;
import com.example.princemidha.proawareness.interfaces.AdapterCommunication;
import com.example.princemidha.proawareness.models.ContactList;

import java.util.List;

/**
 * Created by princemidha on 07/07/17.
 */

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.ViewHolder> {
    private List<ContactList> contactList;
    private static AdapterCommunication mCommunication;
    public ContactsListAdapter(List<ContactList> list) {
        contactList = list;
    }
    @Override
    public ContactsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        mCommunication = (AdapterCommunication)parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mContactName.setText(contactList.get(position).getName());
        holder.position = position;
    }
    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mContactName;
        public Button mDelete;
        public int position = -1;

        public ViewHolder(View v) {
            super(v);
            mContactName = (TextView) v.findViewById(R.id.tv_contactName);
            mDelete = (Button)v.findViewById(R.id.bt_delete);
            mDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mCommunication.getRemovedPosition(position);
        }
    }
}
