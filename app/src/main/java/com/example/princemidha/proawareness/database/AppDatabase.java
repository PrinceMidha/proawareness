package com.example.princemidha.proawareness.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.princemidha.proawareness.models.ContactList;

/**
 * Created by PrinceMidha on 07/07/17.
 */

@Database(entities = {ContactList.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ContactListDao contactListDao();
}
