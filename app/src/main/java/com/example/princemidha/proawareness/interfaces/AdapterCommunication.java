package com.example.princemidha.proawareness.interfaces;

/**
 * Created by princemidha on 07/07/17.
 */

public interface AdapterCommunication {
    public void getRemovedPosition(int position);
}
