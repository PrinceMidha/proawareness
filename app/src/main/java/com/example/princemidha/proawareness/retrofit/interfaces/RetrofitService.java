package com.example.princemidha.proawareness.retrofit.interfaces;




import com.example.princemidha.proawareness.models.BaseModel;
import com.example.princemidha.proawareness.models.ContactList;
import com.example.princemidha.proawareness.models.ResponseData;
import com.example.princemidha.proawareness.retrofit.RetrofitConstants;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by PrinceMidha on 07/07/17.
 */
public interface RetrofitService {

    @POST(RetrofitConstants.CONTACTS_LIST)
    Call<ResponseData> getContactsList(@Header("token") String token);
}



