package com.example.princemidha.proawareness.retrofit;


/**
 * Created by PrinceMidha on 07/07/17.
 */
public class RetrofitConstants {

    public static final String BASE_URL = "http://139.162.152.157/";

    public static final String CONTACTS_LIST = "contactlist.php";
    public static final String TOKEN = "c149c4fac72d3a3678eefab5b0d3a85a";



    public enum MODE {
        CONTACTS_LIST
    }

    public interface LOGIN_PARAMS {
        String IPIN = "email";
        String PASSWORD = "password";
        String CURRENT_PASSWORD = "current_password";
        String PLAYER_ID = "player_id";
        String ONE_SIGNAL_TOKEN = "one_signal_token";
    }
}
