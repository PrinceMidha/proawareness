package com.example.princemidha.proawareness;

import android.content.Context;
import android.util.Log;

import com.example.princemidha.proawareness.models.BaseModel;
import com.example.princemidha.proawareness.models.ContactList;

/**
 * Created by princemidha on 07/07/17.
 */

public class AppUtils {

    public static boolean isValid(Context context, Object responseBody) {
        if (!(responseBody instanceof BaseModel)) {
            return false;
        }
        if (!((BaseModel)responseBody).getStatus().equalsIgnoreCase(context.getString(R.string.success))){
            return false;
        }
        return true;
    }
}
