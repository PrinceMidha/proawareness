package com.example.princemidha.proawareness.AsyncProcess;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.princemidha.proawareness.database.AppDatabase;
import com.example.princemidha.proawareness.database.DatabaseHelper;
import com.example.princemidha.proawareness.interfaces.WorkerToUI;
import com.example.princemidha.proawareness.models.ContactList;

import java.util.List;
import java.util.Set;

/**
 * Created by princemidha on 07/07/17.
 */
public class DBAsyncProcess extends AsyncTask<List<ContactList>,Void,long[]> {
    private final Context context;
    private WorkerToUI workerToUI;
    private AppDatabase database;

    public DBAsyncProcess(Context context, WorkerToUI workerToUI) {
        this.workerToUI = workerToUI;
        this.context = context;
        database = DatabaseHelper.getInstance(context).getDb();
    }

    @Override
    protected long[] doInBackground(List<ContactList>... params) {
        return database.contactListDao().insertAll(params[0]);
    }

    @Override
    protected void onPostExecute(long[] longs) {
        super.onPostExecute(longs);
        workerToUI.doneInBackground(200);
    }
}
