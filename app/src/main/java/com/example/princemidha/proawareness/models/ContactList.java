package com.example.princemidha.proawareness.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Entity(tableName = "Contacts")
public class ContactList {

    @SerializedName("name")
    @Expose
    private String name;

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "uid")
    @SerializedName("uid")
    @Expose
    private String uid;

    @ColumnInfo(name = "status")
    private int isStatus = 1;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getIsStatus() {
        return isStatus;
    }

    public void setIsStatus(int isStatus) {
        this.isStatus = isStatus;
    }
}
