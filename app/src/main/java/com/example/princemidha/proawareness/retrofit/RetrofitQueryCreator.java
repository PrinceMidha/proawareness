package com.example.princemidha.proawareness.retrofit;

import android.content.Context;

import com.example.princemidha.proawareness.models.BaseModel;
import com.example.princemidha.proawareness.models.ContactList;
import com.example.princemidha.proawareness.models.ResponseData;
import com.example.princemidha.proawareness.retrofit.interfaces.RetrofitResponse;
import retrofit2.Call;


/**
 * Created by PrinceMidha on 07/07/17.
 */
public class RetrofitQueryCreator {

    private static RetrofitQueryCreator mRetrofitQueryCreator;

    private RetrofitQueryCreator(){

    }

    public static RetrofitQueryCreator getCreator() {
        if (null == mRetrofitQueryCreator) {
            mRetrofitQueryCreator = new RetrofitQueryCreator();
        }
        return mRetrofitQueryCreator;
    }


    public void  createQuery(Context context, RetrofitResponse callback, RetrofitConstants.MODE mode, boolean showProgress) {
        switch (mode) {
            case CONTACTS_LIST:
                Call<ResponseData> contactListCall = RetrofitManager.init().getRetrofitService().getContactsList(RetrofitConstants.TOKEN);
                contactListCall.enqueue(new RetrofitCustomCallback<ResponseData>(context, callback, showProgress,mode));
                break;
        }

    }
}
