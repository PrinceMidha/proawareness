package com.example.princemidha.proawareness.retrofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;


import com.example.princemidha.proawareness.R;
import com.example.princemidha.proawareness.retrofit.interfaces.RetrofitResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by PrinceMidha on 07/07/17.
 */
public class RetrofitCustomCallback<T> implements Callback<T> {
    private static String defaultMessage = null;
    private RetrofitResponse mRetrofitResponse;
    private Callback callback;
    private Context mCtx;
    private RetrofitConstants.MODE mode;
    private boolean showProgress;
    private ProgressDialog mProgressDialog;
    private String TAG = "RetrofitCustomCallback";
    /**
     * Description : Callback with custom message
     */
    @SuppressWarnings("unchecked")
    public RetrofitCustomCallback(Context context, RetrofitResponse retrofitCallback, boolean showProgress, RetrofitConstants.MODE mode) {
        this.mRetrofitResponse = retrofitCallback;
        defaultMessage = context.getResources().getString(R.string.loading);
        this.mCtx = context;
        this.mode = mode;
        this.showProgress = true;
        if (showProgress) {
            setShowProgress();
        }
    }

//    public RetrofitCustomCallback(Context context, Callback retrofitCallback, boolean showProgress, RetrofitConstants.MODE mode) {
//        this.callback = retrofitCallback;
//        defaultMessage = context.getResources().getString(R.string.loading);
//        this.mCtx = context;
//        this.mode = mode;
//        this.showProgress = true;
//        if (showProgress) {
//            setShowProgress();
//        }
//    }

    private void setShowProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(mCtx, "", defaultMessage);
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        dismissProgressDialog();
      //  Log.d(TAG, "onResponse: ");

        if (response.isSuccessful()) {
           // Log.d(TAG, "onResponse: Successfull");
            if (mRetrofitResponse != null) {
               /// Log.d(TAG, "onResponse: Successfull... mRetrofitResponse not null ");
                mRetrofitResponse.onSuccess(response, mode);
            } else if (callback != null) {
              //  Log.d(TAG, "onResponse: Successfull... callback not null");
                callback.onResponse(call, response);
            }
        } else {
           // Log.d(TAG, "onResponse: Failed ");
            if (mRetrofitResponse != null) {
             //   Log.d(TAG, "onResponse: Failed mRetrofitResponse not null");
                mRetrofitResponse.onFailure(response, mode);
            }
        }
    }

    @Override
    public void onFailure(Call<T> response, Throwable t) {
        dismissProgressDialog();
        if (callback != null){
            callback.onFailure(response,t);return;
        }
        Toast.makeText(mCtx, t.getMessage(), Toast.LENGTH_SHORT).show();
        return;

    }
}
